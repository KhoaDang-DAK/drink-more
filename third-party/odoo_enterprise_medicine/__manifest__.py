# -*- coding: utf-8 -*-
{
    'name': "Odoo Enterprise Medicine",

    'summary': "Thuốc cho Odoo Enterprise",

    'description': """
        Thuốc cho Odoo Enterprise
    """,

    'author': "Orenda: GiangPH",
    'website': "https://orenda.vn",

    # for the full list
    'category': 'Hidden',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'web_enterprise'],

    # always loaded
    'data': [
        'data/inject_medicine.xml',
    ],
}
