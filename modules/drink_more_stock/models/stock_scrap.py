from odoo import api, fields, models


class StockScrapInherit(models.Model):
    _inherit = 'stock.scrap'

    reason = fields.Text('Lý do')
    product_tmpl_id = fields.Many2one('product.template', 'Sản phẩm')
    product_id = fields.Many2one(required=False)
    product_uom_id = fields.Many2one(required=False)

    @api.onchange('product_tmpl_id')
    def onchange_product_tmpl_id(self):
        self.product_id = self.product_tmpl_id.product_variant_id
        self.product_uom_id = self.product_tmpl_id.uom_id
