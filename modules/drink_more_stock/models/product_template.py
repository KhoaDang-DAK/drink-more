from odoo import api, fields, models
from odoo.addons import decimal_precision as dp


class ProductTemplateInherit(models.Model):
    _inherit = 'product.template'
    _rec_name = 'name'

    standard_price = fields.Float(string='Giá nhập', store=True, digits=dp.get_precision('Product Unit of Measure'))
    min_pack_qty = fields.Float(string='SL mua tối thiểu/SP', digits=dp.get_precision('Product Unit of Measure'))

    @api.multi
    def name_get(self):
        return [(product.id, f"{product.default_code} - {product.name} - {product.qty_available} {product.uom_id.name}")
                for product in self]
