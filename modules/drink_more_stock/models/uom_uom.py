from odoo import api, fields, models

class UomInherit(models.Model):
    _inherit = 'uom.uom'

    active = fields.Boolean(defaule=True)

    @api.model
    def delete_default_uom(self):
        #set active=false cho toàn bộ uom
        query_false = """UPDATE uom_uom
                        SET active = false
                        WHERE id not in (11,10,17,4,3,8)"""
        self.env.cr.execute(query_false)

        #Xóa toàn bộ uom_category
        query_ml = """
        DELETE FROM uom_category
        """
        self.env.cr.execute(query_false)

        # Thêm category cái
        query_cai = """                 
                        INSERT INTO uom_category (name,measure_type)
                        VALUES
                            ('Cái', 'cai');
        		"""
        self.env.cr.execute(query_cai)
        # tim kiem id category cái
        query_cai_id = """
                        SELECT id FROM uom_category WHERE measure_type = 'cai'
                		"""
        self.env.cr.execute(query_cai_id)
        cai_id = self.env.cr.dictfetchall()

        # Thêm category Bộ
        query_bo = """                 
                                INSERT INTO uom_category (name,measure_type)
                                VALUES
                                    ('Bộ', 'bo');
                		"""
        self.env.cr.execute(query_bo)
        # tim kiem id category bo
        query_bo_id = """
                        SELECT id FROM uom_category WHERE measure_type = 'bo'
                       		"""
        self.env.cr.execute(query_bo_id)
        bo_id = self.env.cr.dictfetchall()

        # Thêm đơn vị bộ, cái
        query_uom_new = """                 
                        INSERT INTO uom_uom (name,category_id,factor,rounding,active,uom_type,measure_type)
                        VALUES
                            ('Cái', %s, 1, 0.001, true , 'reference', 'cai'),
                            ('Bộ', %s, 1, 0.001, true , 'reference', 'bo');
                """ % (cai_id[0]['id'], bo_id[0]['id'])
        self.env.cr.execute(query_uom_new)



