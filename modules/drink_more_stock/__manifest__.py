# -*- coding: utf-8 -*-
{
    'name': "Drink More: Stock",

    'summary': """
        Module kho của project Drink More""",

    'description': """
        Module kho của project Drink More
    """,

    'author': "Khoa Đặng",
    'website': "khoadang.dak@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','drink_more_sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/uom_data.xml',
        'data/ir_sequence_data.xml',
        'views/product_template.xml',
        'views/stock_inventory.xml',
        'views/stock_picking.xml',
        'views/stock_scrap.xml',
        'views/menu.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}