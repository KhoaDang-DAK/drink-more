from odoo import api, fields, models, http,modules,SUPERUSER_ID
from odoo.http import request
import logging
import json

_logger = logging.getLogger(__name__)


class TestApi(http.Controller):

    @http.route('/testapi', auth='public')
    def testing_api(self):
        # return 'test api success'
        partner = request.env['res.partner'].search([],limit=1)
        return json.dumps({'name': partner.name})

    @http.route(['/search_partner/<dbname>/<id>'],type='http',auth='none',sitemap=False,cors='*',csrf=False)
    def search_partner(self,dbname,id,**kw):
        try:
            registry = modules.registry.Registry(dbname)
            with api.Environment.manage(),registry.cursor() as cr:
                env = api.Environment(cr,SUPERUSER_ID,{})
                rec = env['res.partner'].search([('id','=',int(id))],limit=1)
                response = {
                    'status':'ok',
                    'content':{
                        'name':rec.name,
                        'mobile':rec.mobile,
                    }
                }
        except Exception:
            response = {
                'status':'error',
                'content':'not found',
            }
        return json.dumps(response)
