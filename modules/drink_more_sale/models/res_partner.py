from odoo import api, fields, models


class ResPartnerInherit(models.Model):
    _inherit = 'res.partner'

    # Thêm trường mới
    taste = fields.Text(string='Khẩu vị', track_visibility='onchange')
    membership_card = fields.Char(string='Mã thẻ thành viên', track_visibility='onchange')
    gender = fields.Selection(string='Giới tính', selection=[('female', 'Nữ'), ('male', 'Nam')],
                              track_visibility='onchange')
    _sql_constraints = [('unique_mobile', 'unique(mobile)', 'Số điện thoại này đã tồn tại trên hệ thống')]
