from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp


class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'
    _order = 'id desc'

    # Sửa trường có sẵn
    state = fields.Selection(
        selection=[('draft', 'Nháp'), ('sale', 'Đang giao'), ('done', 'Hoàn thành'), ('cancel', 'Hủy')])
    partner_id = fields.Many2one(required=False)

    # Thêm trường mới
    customer_phone = fields.Char(string='Số điện thoại', track_visibility='onchange')
    shipping_address = fields.Char(string='Địa chỉ giao hàng', track_visibility='onchange')
    customer_taste = fields.Text(string='Khẩu vị', track_visibility='onchange')
    note = fields.Text(string='Ghi chú', track_visibility='onchange')
    surcharge = fields.Float(string='Phụ phí', track_visibility='onchange')
    payment_method = fields.Selection(string='Phương thức thanh toán',
                                      selection=[('cash', 'Tiền mặt'), ('bank', 'Tài khoản ngân hàng')], default='cash',
                                      track_visibility='onchange')
    shipping_method = fields.Selection(string='Phương thức giao hàng',
                                       selection=[('shipper', 'Dịch vụ giao hàng'), ('self', 'Tự giao hàng'),
                                                  ('guest_take', 'Khách tự qua lấy')], default='shipper',
                                       track_visibility='onchange')
    customer_name = fields.Char('Khách hàng')
    save_info = fields.Boolean('Lưu lại thông tin', track_visibility='onchange')
    is_new_customer = fields.Boolean(default=False)
    discount_total = fields.Float(string='Tổng giảm giá', digits=dp.get_precision('Product Unit of Measure'),
                                  compute='compute_discount_total', store=True)


    @api.depends('order_line.cash_discount')
    def compute_discount_total(self):
        for record in self:
            record.discount_total = sum(record.order_line.mapped('cash_discount'))

    @api.model
    def set_false_vals(self):
        self.partner_id = False
        self.shipping_address = False
        self.customer_taste = False
        self.membership_card = False
        self.is_new_customer = True

    @api.onchange('customer_phone')
    def onchange_customer_phone(self):
        if not self.save_info and self.state == 'draft':
            if self.customer_phone:
                customer = self.env['res.partner'].search([('mobile', '=', self.customer_phone)])
                if customer:
                    self.partner_id = customer.id
                    self.shipping_address = customer.street
                    self.customer_taste = customer.taste
                    self.membership_card = customer.membership_card
                    self.is_new_customer = False
                else:
                    self.set_false_vals()
            else:
                self.set_false_vals()

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            self.customer_phone = self.partner_id.mobile
            self.shipping_address = self.partner_id.street
            self.customer_taste = self.partner_id.taste
            self.membership_card = self.partner_id.membership_card
            self.is_new_customer = False
        else:
            self.set_false_vals()

    @api.model
    def create(self, vals):
        if vals.get('name', _('New') == _('New')):
            sequence = self.env.ref('drink_more_sale.sale_order_sequence').next_by_id()
            vals['name'] = sequence
        if not vals.get('partner_id'):
            new_cus = self.env['res.partner'].sudo().create(
                {'name': vals.get('customer_name'), 'street': vals.get('shipping_address'),
                 'mobile': vals.get('customer_phone'),
                 'taste': vals.get('customer_taste')})
            vals['partner_id'] = new_cus.id
            vals['is_new_customer'] = False
        if vals.get('save_info'):
            customer = self.env['res.partner'].browse(vals.get('partner_id'))
            customer.write(
                {'mobile': vals.get('customer_phone'), 'street': vals.get('shipping_address'),
                 'taste': vals.get('customer_taste')})
        result = super(SaleOrderInherit, self).create(vals)
        return result

    @api.multi
    def action_done(self):
        for order in self:
            tx = order.sudo().transaction_ids.get_last_transaction()
            if tx and tx.state == 'pending' and tx.acquirer_id.provider == 'transfer':
                tx._set_transaction_done()
                tx.write({'is_processed': True})
        self.picking_ids.button_validate()
        return self.write({'state': 'done'})

    @api.onchange('surcharge')
    def onchange_surcharge(self):
        self._amount_all()

    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_unit * line.product_uom_qty
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': amount_untaxed,
                'amount_tax': amount_tax,
                'amount_total': (amount_untaxed + amount_tax + order.surcharge) - order.discount_total,
            })
