from odoo import api, fields, models
from odoo.addons import decimal_precision as dp


class SaleOrderLineInherit(models.Model):
    _inherit = 'sale.order.line'

    # Sửa lại trường đã có
    price_unit = fields.Float(compute="compute_price_unit", store=True)
    name = fields.Char(required=False)

    # Thêm trường mới
    product_tmpl_id = fields.Many2one('product.template', 'Sản phẩm')
    qty_available = fields.Float('SL thực', digits=dp.get_precision('Product Unit of Measure'),
                                 related='product_tmpl_id.qty_available')
    note = fields.Char('Ghi chú')
    cash_discount = fields.Float(string='Giảm giá(VND)', digits=dp.get_precision('Product Unit of Measure'))

    @api.onchange('cash_discount')
    def validate_discount(self):
        if self.cash_discount > self.product_tmpl_id.list_price:
            self.cash_discount = self.product_tmpl_id.list_price
        elif self.cash_discount < 0:
            self.cash_discount = 0

    @api.onchange('product_tmpl_id')
    def onchange_product_tmpl_id(self):
        self.product_id = self.product_tmpl_id.product_variant_id
        self.product_uom = self.product_tmpl_id.uom_id
        current_product_ids = self.order_id.order_line.mapped('product_tmpl_id').mapped('id')
        return {'domain': {'product_tmpl_id': [('id', 'not in', current_product_ids)]}}

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
                                            product=line.product_id, partner=line.order_id.partner_shipping_id)
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'] - line.cash_discount,
            })

    @api.depends('product_tmpl_id')
    def compute_price_unit(self):
        self.price_unit = self.product_tmpl_id.list_price

    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        if self.order_id.pricelist_id and self.order_id.partner_id:
            product = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.partner_id,
                quantity=self.product_uom_qty,
                date=self.order_id.date_order,
                pricelist=self.order_id.pricelist_id.id,
                uom=self.product_uom.id,
                fiscal_position=self.env.context.get('fiscal_position')
            )
            self.price_unit = self.env['account.tax']._fix_tax_included_price_company(self._get_display_price(product),
                                                                                      product.taxes_id, self.tax_id,
                                                                                      self.company_id)

    @api.multi
    def _compute_tax_id(self):
        for line in self:
            line.tax_id = None