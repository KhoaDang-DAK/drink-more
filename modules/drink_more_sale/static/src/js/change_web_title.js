odoo.define('gpp.AbstractWebClient', function (require) {
    "use strict";

    var concurrency = require('web.concurrency');
    var KeyboardNavigationMixin = require('web.KeyboardNavigationMixin');
    var ServiceProviderMixin = require('web.ServiceProviderMixin');

    var AbstractWebClient = require('web.AbstractWebClient');

    AbstractWebClient.include({
        init: function (parent) {
            this._super.apply(this, arguments);
            this.set('title_part', {"zopenerp": "Drink More"});
        },
    });
});