# -*- coding: utf-8 -*-


{
    'name': "Drink More: Bán hàng",

    'summary': """
        Module bán hàng của project Drink More""",

    'description': """
        Module bán hàng của project Drink More 
    """,

    'author': "Khoa Đặng",
    'website': "khoadang.dak@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Wholesale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'sale', 'sale_management', 'sale_stock', 'odoo_web_login12'],

    # always loaded
    'data': [
        'security/group.xml',
        'data/ir_sequence_data.xml',
        'views/web_template.xml',
        'views/js_asset.xml',
        'views/sale_order.xml',
        # 'views/res_partner.xml',
        'views/menu.xml',
    ],
    "installable": True,
    "application": True,
}
