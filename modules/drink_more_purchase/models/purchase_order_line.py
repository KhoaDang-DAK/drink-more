from odoo import api, fields, models, exceptions
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.onchange('product_qty')
    def change_min_product_qty(self):
        if self.product_qty < self.product_id.product_tmpl_id.min_pack_qty:
            self.product_qty = self.product_id.product_tmpl_id.min_pack_qty

    @api.onchange('product_id')
    def onchange_product_id_domain(self):
        return {'domain': {'product_id': [
            ('id', 'not in', self.order_id.mapped('order_line').mapped('product_id').ids),
            ('purchase_ok', '=', True)]}}

    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result

        # Reset date, price and quantity since _onchange_quantity will provide default values
        self.date_planned = datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        # self.price_unit = self.product_qty = 0.0
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        result['domain'] = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}

        product_lang = self.product_id.with_context(
            lang=self.partner_id.lang,
            partner_id=self.partner_id.id,
        )
        self.name = product_lang.display_name
        if product_lang.description_purchase:
            self.name += '\n' + product_lang.description_purchase

        # self._compute_tax_id()
        self.product_uom = self.product_id.product_tmpl_id.uom_id
        self.price_unit = self.product_id.product_tmpl_id.standard_price
        self._suggest_quantity()
        self._onchange_quantity()
        return result

    @api.onchange('product_qty', 'product_uom')
    def _onchange_quantity(self):
        if not self.product_id:
            return
        params = {'order_id': self.order_id}
        seller = self.product_id._select_seller(
            partner_id=self.partner_id,
            quantity=self.product_qty,
            date=self.order_id.date_order and self.order_id.date_order.date(),
            uom_id=self.product_uom,
            params=params)

        if seller or not self.date_planned:
            self.date_planned = self._get_date_planned(seller).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        if not seller:
            if self.product_id.seller_ids.filtered(lambda s: s.name.id == self.partner_id.id):
                self.price_unit = 0.0
            return

        price_unit = self.env['account.tax']._fix_tax_included_price_company(seller.price,
                                                                             self.product_id.supplier_taxes_id,
                                                                             self.taxes_id,
                                                                             self.company_id) if seller else 0.0
        if price_unit and seller and self.order_id.currency_id and seller.currency_id != self.order_id.currency_id:
            price_unit = seller.currency_id._convert(
                price_unit, self.order_id.currency_id, self.order_id.company_id, self.date_order or fields.Date.today())

        # if seller and self.product_uom and seller.product_uom != self.product_uom:
        #     price_unit = seller.product_uom.with_context(uom=self.product_uom.id)._compute_price(price_unit, self.product_uom)

        self.price_unit = price_unit

    def _suggest_quantity(self):
        '''
        Suggest a minimal quantity based on the seller
        '''
        if not self.product_id:
            return
        # seller_min_qty = self.product_id.seller_ids \
        #         #     .filtered(
        #         #     lambda r: r.name == self.order_id.partner_id and (not r.product_id or r.product_id == self.product_id)) \
        #         #     .sorted(key=lambda r: r.min_qty)
        #         # if seller_min_qty:
        #         #     self.product_uom = seller_min_qty[0].product_uom
        #         #     print(self.product_uom)
        #         # else:
        #         #     self.product_qty = 1.0
        self.product_qty = self.product_id.product_tmpl_id.min_pack_qty
        self.product_uom = self.product_id.product_tmpl_id.uom_id