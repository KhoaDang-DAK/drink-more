from odoo import api, fields, models, exceptions


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def action_view_picking(self):
        """ This function returns an action that display existing picking orders of given purchase order ids. When only one found, show the picking immediately.
        """
        action = self.env.ref('drink_more_purchase.popup_receipt_picking_action')
        result = action.read()[0]
        # override the context to get rid of the default filtering on operation type
        pick_ids = self.mapped('picking_ids')
        # choose the view_mode accordingly
        if not pick_ids or len(pick_ids) > 1:
            result['domain'] = "[('id','i   n',%s)]" % (pick_ids.ids)
        elif len(pick_ids) == 1:
            res = self.env.ref('drink_more_purchase.popup_receipt_picking_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = pick_ids.id
            result['target'] = 'new'
        return result
