# -*- coding: utf-8 -*-
{
    'name': "Drink More: Mua hàng",

    'summary': """
        Module mua hàng của project Drink More""",

    'description': """
        Module mua hàng của project Drink More
    """,

    'author': "Khoa Đặng",
    'website': "khoadang.dak@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'purchase', 'purchase_stock'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/stock_picking.xml',
        'views/purchase_order.xml',
        'views/menu.xml',
        # 'views/menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
}
