from odoo import api, fields, models


class JavascriptReport(models.Model):
    _name = 'js.report'

    @api.model
    def get_list_inventory(self):
        wh = self.env['stock.warehouse'].search([])
        result = []
        for w in wh:
            result.append({
                'id': w.id,
                'name': w.name,
            })
        return result

    @api.model
    def get_list_category(self):
        category = self.env['product.category'].search([])
        result = []
        for categ in category:
            result.append({
                'id': categ.id,
                'name': categ.name,
            })
        return result

    @api.model
    def get_list_product(self, product_type=None, product_categ=None):
        domain = []
        if product_type and product_type != 'all':
            domain.append(('type', '=', product_type))
        if product_categ and int(product_categ) != -1:
            domain.append(('categ_id', '=', product_categ))
        products = self.env['product.template'].search(domain)
        result = []
        for product in products:
            result.append({
                'id': product.id,
                'name': f'{product.default_code} - {product.name}'
            })
        return result
