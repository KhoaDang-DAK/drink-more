from odoo import api, fields, models
from datetime import datetime


class JsReprotInventoryInOut(models.Model):
    _inherit = 'js.report'

    @api.model
    def report_inventory_about_time(self, date_from, date_to, product_tmpl_id, wh=False, prd_categ=False,
                                    prd_type=False):
        date_from = datetime.strptime(date_from, '%d/%m/%Y')
        date_to = datetime.strptime(date_to, '%d/%m/%Y')
        lines = {}
        product_tmpl_id = int(product_tmpl_id) or 0
        if product_tmpl_id > 0:
            product = self.env['product.template'].browse(product_tmpl_id)
            product_ids, product_ids_str = [int(product_tmpl_id)], "({0})".format(product_tmpl_id)
        else:
            domain = []
            if prd_categ and int(prd_categ) > 0:
                domain.append(('categ_id', '=', int(prd_categ)))
            if prd_type and int(prd_type)>0:
                domain.append()
