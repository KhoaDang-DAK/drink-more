odoo.define('drink_more_report.report_inventory_in_out', function (require) {
    "use strict";

    var core = require("web.core");
    var AbstractAction = require('web.AbstractAction');
    var qweb = core.qweb;
    var rpc = require("web.rpc");
    var date_picker = require('web.datepicker');
    var selected_type;
    var selected_categ;
    var selected_product;
    var selected_wh;
    var date_from;
    var date_to;
    var curr_date;

    function string_to_date(str_date) {
        var parts = str_date.split('-');
        return new Date(parts[0], parts[1] - 1, parts[2]);
    }

    var report_inventory_in_out = AbstractAction.extend({
        template: "T_ReportInventoryInOut",
        events: {
            'change .list-type-container select': 'render_type_category_and_product',
            'click .list-type-container select': 'render_type_category_and_product',
            'click .list-category-container select': 'callback_render_category_select',
            'change .list-category-container select': 'callback_render_category_product_select',
            'click .list-inventory-container select': 'callback_render_inventory_select',
            'click #submit-about-time': 'get_data_inventory_about_time',
            'change .list-product-container': 'render_type_category_and_product',
            'click .list-product-container': 'render_type_category_and_product',
        },

        render_category_select: function () {
            rpc.query({
                model: 'js.report',
                method: 'get_list_category',
                args: [],
            }).then(function (result) {
                $('.list-category-container select').select2(
                    _.each(result, function (result) {
                        $('.list-category-container select').append('<option value="' + result.id + '">' + result.name + '</option>')
                    })
                );
            });
        },

        render_type_select: function () {
            let product_type = $('.list-type-container select').val();
            $('.list-category-container select').select2('destroy');
            $('.list-category-container select').html(
                '<option selected value=-1>Tất cả nhóm</option>'
            );
            rpc.query({
                model: 'js.report',
                method: 'get_list_product',
                args: [product_type],
            }).then(function (result) {
                $('.list-product-container select').select2('destroy');
                $('.list-product-container select').html(
                    '<option selected value=-1>Tất cả sản phẩm</option>'
                );
                _.each(result, function (result) {
                    console.log(result.id, result.name)
                    $('.list-product-container select').append('<option value="' + result.id + '">' + result.name + '</option>')
                });
            });
        },

        render_category_product_select: function () {
            let product_type = $('.list-type-container select').val();
            let product_categ = $('.list-category-container select').val();
            rpc.query({
                model: 'js.report',
                method: 'get_list_product',
                args: [product_type, product_categ],
            }).then(function (result) {
                $('.list-product-container select').select2('destroy');
                $('.list-product-container select').html(
                    '<option selected value=-1>Tất cả sản phẩm</option>'
                );
                _.each(result, function (result) {
                    console.log(result.id, result.name)
                    $('.list-product-container select').append('<option value="' + result.id + '">' + result.name + '</option>')
                });
            });
        },

        render_inventory_select: function () {
            rpc.query({
                model: 'js.report',
                method: 'get_list_inventory',
            }).then(function (result) {
                $('.list-inventory-container select').select2();
                _.each(result, function (result) {
                    console.log(result.id, result.name)
                    $('.list-inventory-container select').append('<option value="' + result.id + '">' + result.name + '</option>')
                });
            });
        },

        callback_render_category_product_select: function () {
            this.render_category_product_select();
        },

        callback_render_category_select: function () {
            this.render_category_select();
        },

        render_type_category_and_product: function () {
            this.render_type_select();
        },

        callback_render_inventory_select: function () {
            this.render_inventory_select();
        },

        start: function () {
            var first_day = moment().format('DD/MM/YYYY');
            var today = moment().format('DD/MM/YYYY');
            var date_picker_option = {
                calendarWeeks: true,
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    next: 'fa fa-chevron-right',
                    previous: 'fa fa-chevron-left',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    close: 'fa fa-times',
                },
                format: 'DD/MM/YYYY',
            };
            var dt_from = new date_picker.DateWidget(date_picker_option);
            var dt_to = new date_picker.DateWidget(date_picker_option);
            dt_from.replace(this.$("#datepicker-from input"));
            dt_to.replace(this.$("#datepicker-to input"));
            dt_from.$el.find('input').addClass('form-control');
            dt_to.$el.find('input').addClass('form-control');
            dt_from.$el.find('input').val(first_day);
            dt_to.$el.find('input').val(today);

            date_from = first_day;
            date_to = today;
            selected_wh = -1;
            selected_product = -1;

            var self = this;
            this.callback_render_category_product_select();
            this.callback_render_category_select();
            this.render_type_category_and_product();
            this.callback_render_inventory_select();
        },

        get_data_inventory_about_time: function () {
            selected_type = this.$('#type').val();
            selected_categ = this.$('#category').val();
            selected_product = this.$('#product').val();
            selected_wh = this.$('#stock').val();
            date_from = this.$('#datepicker-from input').val();
            date_to = this.$('#datepicker-to input').val();
            curr_date = moment().format('DD/MM/YYYY');
            if (date_from === "" || date_to === "") {
                alert("Ngày bắt đầu hoặc ngày kết thúc không được bỏ trống, vui lòng nhập đầy đủ!");
                return;
            } else {
                if (string_to_date(date_from) > string_to_date(date_to)) {
                    alert("Ngày bắt đầu không thể lớn hơn ngày kết thúc!");
                    return;
                } else if (string_to_date(date_from) > string_to_date(curr_date)) {
                    alert("Ngày bắt đầu không thể lớn hơn ngày hiện tại!");
                    return;
                } else {
                    this.$("#submit-about-time").attr('disabled', 'disabled');
                    this.$("#submit-inventory-about-time-report").attr('disabled', 'disabled');
                    this._rpc({
                        model: 'js.report',
                        method: 'report_inventory_about_time',
                        args: [date_from, date_to, selected_product, selected_wh, selected_categ, selected_type],
                    })
                }
            }
        },
    });

    core.action_registry.add('drink_more_report.report_inventory_in_out', report_inventory_in_out);
});