# -*- coding: utf-8 -*-
{
    'name': "Drink More: Báo Cáo",

    'summary': """
        Module báo cáo drink_more""",

    'description': """
        Module báo cáo drink_more
    """,

    'author': "Khoa Đặng",
    'website': "khoadang.dak@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/report_inventory.xml',
        'views/menu.xml',
        'views/js_asset.xml'
        # 'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'qweb': [
        'static/xml/*',
    ],
    "installable": True,
    "application": True,
}
